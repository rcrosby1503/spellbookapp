package bob.pathfinderspellbook;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bob on 10/19/2015.
 */
public class CSVFile extends ArrayAdapter<Spells> {
    Context ctx;

    public CSVFile(Context context, int textViewResourceId){
        super(context, textViewResourceId);

        this.ctx = context;

        loadArrayFromFile();
    }
    @Override
    public View getView(final int pos, View convertView, final ViewGroup parent){
        TextView mView = (TextView)convertView;

        if(mView == null){
            mView = new TextView(parent.getContext());
            mView.setTextSize(28);
        }

        mView.setText(getItem(pos).getName());

        return mView;
    }

    private void loadArrayFromFile(){
        try{
            //InputStream is = ctx.getAssets().open("spell_list.csv");
            InputStream is = ctx.getResources().openRawResource(R.raw.spell_list_1);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;

            while((line = reader.readLine()) != null){
                String rowData[] = line.split(",");

                Spells cur = new Spells();
                cur.setName(rowData[0]);
                cur.setSchool(rowData[1]);
                cur.setSubSchool(rowData[2]);
                cur.setDescriptor(rowData[3]);
                cur.setSpellLevel(rowData[4]);
                cur.setCastingTime(rowData[5]);
                cur.setComponents(rowData[6]);
                cur.setCostlyComponents(rowData[7]);
                cur.setRange(rowData[8]);
                cur.setArea(rowData[9]);
                cur.setEffect(rowData[10]);
                cur.setTargets(rowData[11]);
                cur.setDuration(rowData[12]);
                cur.setDismissable(rowData[13]);
                cur.setShapeable(rowData[14]);
                cur.setSavingThrow(rowData[15]);
                cur.setSpellResistence(rowData[16]);
                cur.setDescription(rowData[17]);
                //cur.setShortDescription(rowData[18]);


                this.add(cur);
            }
        }catch(IOException ex){
            //throw new RuntimeException("Error Reading " + ex);
            ex.printStackTrace();
        }
        /*finally{
            try{
                is.close();
            }catch(IOException e){
                throw new RuntimeException("Error closing " + e);
            }
        }*/
    }
}

