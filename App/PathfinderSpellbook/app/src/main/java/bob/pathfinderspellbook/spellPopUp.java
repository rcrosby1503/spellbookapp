package bob.pathfinderspellbook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.text.Editable;

/**
 * Created by bob on 8/21/2015.
 */
public class spellPopUp extends Activity {
    private Button save;
    private String s_charName;
    private String s_charLevel;
    private String s_charInt;
    private int charLevel;
    private int charInt;

    @Override
    protected void onCreate(Bundle savedInstancestate) {
        super.onCreate(savedInstancestate);

        setContentView(R.layout.spell_pop_up);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.8),(int)(height*.8));

        /**** Might need this later.
        save = (Button)findViewById(R.id.b_save_close);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //close popup
                finish();
                //System.exit(0);
            }
        });
        */
    }
}
