package bob.pathfinderspellbook;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.text.Editable;
import android.widget.TextView;


public class SellBook extends ActionBarActivity {
    public double[] scribePrice = {37.5,225,562.5,1050,1687.5,2475,3412.5,4500,5737.5};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_book);
        final Button calcButton = (Button)findViewById(R.id.calcButton);
        final TextView endCalc = (TextView)findViewById(R.id.calc);
        calcButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               /* EditText sp1 = (EditText) findViewById(R.id.spellOne);
                Editable edit = sp1.getText();
                String theSpells = edit.toString();
                int spells = Integer.parseInt(theSpells);
                calcPrice(spells, endCalc);*/
                EditText[] editText = new EditText[9];
                Editable[] edit = new Editable[9];
                String[] levelString = new String[9];
                int[] levelInt = new int[9];
                for (int i = 0; i < 9; i++){
                    int resID = getResources().getIdentifier("spell" + i, "id", getPackageName());
                    editText[i] = (EditText)findViewById(resID);
                    edit[i] = editText[i].getText();
                    levelString[i] = edit[i].toString();
                    levelInt[i] = Integer.parseInt(levelString[i]);
                }
                calcPrice(levelInt, endCalc);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sell_book, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void calcPrice(int[] levelInt, TextView endCalc){
        double totalCost = 0;

        for (int i = 0; i < 9; i++){
            totalCost += (levelInt[i] * scribePrice[i]);
        }
        totalCost = totalCost / 2;

        endCalc.setText("" + totalCost);
    }


}
