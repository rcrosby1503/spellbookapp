package bob.pathfinderspellbook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.text.Editable;

/**
 * Created by bob on 8/21/2015.
 */
public class popUp extends Activity {
    private Button save;
    private String s_charName;
    private String s_charLevel;
    private String s_charInt;
    private int charLevel;
    private int charInt;

    @Override
    protected void onCreate(Bundle savedInstancestate) {
        super.onCreate(savedInstancestate);

        setContentView(R.layout.popupwindow);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.8),(int)(height*.8));


        save = (Button)findViewById(R.id.b_save_close);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText nameEdit = (EditText) findViewById(R.id.char_name);
                EditText intEdit = (EditText) findViewById(R.id.char_int);
                EditText levelEdit = (EditText) findViewById(R.id.char_level);
                //save data to character
                Intent i = getIntent();
                s_charName = nameEdit.getText().toString();
                s_charInt = intEdit.getText().toString();
                s_charLevel = levelEdit.getText().toString();

                charInt = Integer.parseInt(s_charInt);
                charLevel = Integer.parseInt(s_charLevel);

                i.putExtra("s_charName", s_charName);
                i.putExtra("charInt", charInt);
                i.putExtra("charLevel", charLevel);
                setResult(RESULT_OK, i);
                //close popup
                finish();
                //System.exit(0);
            }
        });
    }
}
