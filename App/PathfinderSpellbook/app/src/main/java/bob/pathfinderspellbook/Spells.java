package bob.pathfinderspellbook;

/**
 * Created by Bob on 3/22/2016.
 */
public class Spells {
    String name;
    String school;
    String subSchool;
    String descriptor;
    String spellLevel;
    String castingTime;
    String components;
    String costlyComponents;
    String range;
    String area;
    String effect;
    String targets;
    String duration;
    String dismissable;
    String shapeable;
    String savingThrow;
    String spellResistence;
    String description;
    String shortDescription;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getSubSchool() {
        return subSchool;
    }

    public void setSubSchool(String subSchool) {
        this.subSchool = subSchool;
    }

    public String getDescriptor() {
        return descriptor;
    }

    public void setDescriptor(String descriptor) {
        this.descriptor = descriptor;
    }

    public String getSpellLevel() {
        return spellLevel;
    }

    public void setSpellLevel(String spellLevel) {
        this.spellLevel = spellLevel;
    }

    public String getCastingTime() {
        return castingTime;
    }

    public void setCastingTime(String castingTime) {
        this.castingTime = castingTime;
    }

    public String getComponents() {
        return components;
    }

    public void setComponents(String components) {
        this.components = components;
    }

    public String getCostlyComponents() {
        return costlyComponents;
    }

    public void setCostlyComponents(String costlyComponents) {
        this.costlyComponents = costlyComponents;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public String getTargets() {
        return targets;
    }

    public void setTargets(String targets) {
        this.targets = targets;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDismissable() {
        return dismissable;
    }

    public void setDismissable(String dismissable) {
        this.dismissable = dismissable;
    }

    public String getShapeable() {
        return shapeable;
    }

    public void setShapeable(String shapeable) {
        this.shapeable = shapeable;
    }

    public String getSavingThrow() {
        return savingThrow;
    }

    public void setSavingThrow(String savingThrow) {
        this.savingThrow = savingThrow;
    }

    public String getSpellResistence() {
        return spellResistence;
    }

    public void setSpellResistence(String spellResistence) {
        this.spellResistence = spellResistence;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }
}
