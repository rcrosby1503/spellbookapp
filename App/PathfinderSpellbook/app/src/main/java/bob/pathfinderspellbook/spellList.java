package bob.pathfinderspellbook;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import java.io.InputStream;
import java.util.List;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;


public class spellList extends ActionBarActivity {
    //private ListView listView;
    CSVFile mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spell_list);

        final ListView spellList = (ListView)findViewById(R.id.spellList);

        mAdapter = new CSVFile(this, -1);
        spellList.setAdapter(mAdapter);

        spellList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(view.getContext(), mAdapter.getItem(i).getDescriptor(), Toast.LENGTH_SHORT).show();
                //startPopup();
                View v = LayoutInflater.from(spellList.this).inflate(R.layout.spell_pop_up, null);
                AlertDialog.Builder builder = new AlertDialog.Builder(spellList.this);
                builder.setMessage(mAdapter.getItem(i).getName() + "\n" +
                        "School: " + mAdapter.getItem(i).getSchool() + "\n" +
                        "Level: " + mAdapter.getItem(i).getSpellLevel() + "\n"+
                        "Casting Time: " + mAdapter.getItem(i).getCastingTime() + "\n" +
                        "Components: " + mAdapter.getItem(i).getComponents() + "\n" +
                        "Range: " + mAdapter.getItem(i).getRange() + "\n\n" +
                        mAdapter.getItem(i).getDescription())
                        .setView(v)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which){
                                //do something
                            }
                        })
                        .setCancelable(false);
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_spell_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void startPopup(){
        Intent startPop = new Intent(this, spellPopUp.class);

        //startPop.putExtra("char1Select", char1Select);
        startActivity(startPop);
    }
}
