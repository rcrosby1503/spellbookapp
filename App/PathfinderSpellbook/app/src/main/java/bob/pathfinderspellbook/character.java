package bob.pathfinderspellbook;

/**
 * Created by Bob on 5/12/2015.
 */
public class character {
    private static String charName;
    private static int charLevel;
    private static int charInt;
    private static int addBonus;
    private static int[] maxSpells;
    private static int[] prepSpells;

    public static boolean isNew;

    public character(){
        charName = "New Character";
        isNew = true;
        maxSpells = new int[10];
        prepSpells = new int[10];
        charLevel = 0;
        charInt = 0;
        addBonus = 0;
        for (int i = 0; i < 10; i++){
            maxSpells[i] = 0;
            prepSpells[i] = 0;
        }
    }

    public static int getCharLevel(){
        return charLevel;
    }
    public static void setCharLevel(int level){
        charLevel = level;
    }
    public static int getCharInt(){
        return charInt;
    }
    public static void setCharInt(int intel){
        charInt = intel;
    }
    public static int getAddBonus(){
        return addBonus;
    }
    public static void setAddBonus(int bonus){
        addBonus = bonus;
    }
    public static int getMaxSpells(int index){
        return maxSpells[index];
    }
    public static void setMaxSpells(int index, int max){
        maxSpells[index] = max;
    }
    public static int getPrepSpells(int index){
        return prepSpells[index];
    }
    public static void setPrepSpells(int index, int prep){
        prepSpells[index] = prep;
    }
    public static String getCharName(){return charName;}
    public static void setCharName(String name){charName = name;}
}
