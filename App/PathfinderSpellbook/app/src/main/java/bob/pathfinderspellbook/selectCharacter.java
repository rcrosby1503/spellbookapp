package bob.pathfinderspellbook;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.PopupWindow;
import android.widget.PopupMenu;


public class selectCharacter extends ActionBarActivity {
    public static int EDIT_CHAR = 1010;
    public static character char1;
    public character char2;
    public character char3;
    public character char4;
    public character char5;
    public static boolean char1Select = false;
    public boolean char2Select = false;
    public boolean char3Select = false;
    public boolean char4Select = false;
    public boolean char5Select = false;
    private Button character1, character2, character3, character4, character5;
    private boolean click;

    PopupWindow popUp;
    LinearLayout layout;
    TextView tv;
    LayoutParams params;
    LinearLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //ToDo: initialize characters on main page and send as bundle to this page.
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_character);
        char1 = new character();
        char2 = new character();
        char3 = new character();
        char4 = new character();
        char5 = new character();
        character1 = (Button)findViewById(R.id.char1);
        character2 = (Button)findViewById(R.id.char2);
        character3 = (Button)findViewById(R.id.char3);
        character4 = (Button)findViewById(R.id.char4);
        character5 = (Button)findViewById(R.id.char5);
        character1.setText(char1.getCharName());
        character2.setText(char2.getCharName());
        character3.setText(char3.getCharName());
        character4.setText(char4.getCharName());
        character5.setText(char5.getCharName());


        //Listeners for all the buttons!
        character1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                char1Select = true;
                char2Select = false;
                char3Select = false;
                char4Select = false;
                char5Select = false;
                startPopup();
                //startActivity(new Intent(selectCharacter.this, popUp.class));
            }
        });

        character2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                char1Select = false;
                char2Select = true;
                char3Select = false;
                char4Select = false;
                char5Select = false;
                startPopup();
                //startActivity(new Intent(selectCharacter.this, popUp.class));
            }
        });

        character3.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                char1Select = false;
                char2Select = false;
                char3Select = true;
                char4Select = false;
                char5Select = false;
                startPopup();
                //startActivity(new Intent(selectCharacter.this, popUp.class));
            }
        });

        character4.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                char1Select = false;
                char2Select = false;
                char3Select = false;
                char4Select = true;
                char5Select = false;
                startPopup();
                //startActivity(new Intent(selectCharacter.this, popUp.class));
            }
        });

        character5.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                char1Select = false;
                char2Select = false;
                char3Select = false;
                char4Select = false;
                char5Select = true;
                startPopup();
                //startActivity(new Intent(selectCharacter.this, popUp.class));
            }
        });


    }

    public void startPopup(){
        Intent startPop = new Intent(this, popUp.class);

        startPop.putExtra("char1Select", char1Select);
        startActivityForResult(startPop, EDIT_CHAR);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode == EDIT_CHAR && resultCode == RESULT_OK){
            if(char1Select) {
                char1.setCharName(data.getExtras().getString("s_charName"));
                char1.setCharInt(data.getExtras().getInt("charInt"));
                char1.setCharLevel(data.getExtras().getInt("charLevel"));
                character1.setText(char1.getCharName() + " Lvl: " + char1.getCharLevel());
            }else if(char2Select){
                char2.setCharName(data.getExtras().getString("s_charName"));
                char2.setCharInt(data.getExtras().getInt("charInt"));
                char2.setCharLevel(data.getExtras().getInt("charLevel"));
                character2.setText(char2.getCharName() + " Lvl: " + char2.getCharLevel());
            }else if(char3Select){
                char3.setCharName(data.getExtras().getString("s_charName"));
                char3.setCharInt(data.getExtras().getInt("charInt"));
                char3.setCharLevel(data.getExtras().getInt("charLevel"));
                character3.setText(char3.getCharName() + " Lvl: " + char3.getCharLevel());
            }else if(char4Select){
                char4.setCharName(data.getExtras().getString("s_charName"));
                char4.setCharInt(data.getExtras().getInt("charInt"));
                char4.setCharLevel(data.getExtras().getInt("charLevel"));
                character4.setText(char4.getCharName() + " Lvl: " + char4.getCharLevel());
            }else if(char5Select){
                char5.setCharName(data.getExtras().getString("s_charName"));
                char5.setCharInt(data.getExtras().getInt("charInt"));
                char5.setCharLevel(data.getExtras().getInt("charLevel"));
                character5.setText(char5.getCharName() + " Lvl: " + char5.getCharLevel());
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_select_character, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

   /* public void selectCharacter(){
        if (click) {
            popUp.showAtLocation(mainLayout, Gravity.BOTTOM, 10, 10);
            popUp.update(50, 50, 300, 80);
            click = false;
        }else{
            popUp.dismiss();
            click = true;
        }
        params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        layout.setOrientation(LinearLayout.VERTICAL);
        tv.setText("Hi this is a sample text for popup window");
        layout.addView(tv, params);
        popUp.setContentView(layout);
        // popUp.showAtLocation(layout, Gravity.BOTTOM, 10, 10);
        mainLayout.addView(character1, params);
        setContentView(mainLayout);
    }*/
}


