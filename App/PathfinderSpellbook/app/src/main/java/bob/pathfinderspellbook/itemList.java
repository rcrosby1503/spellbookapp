package bob.pathfinderspellbook;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bob on 10/14/2015.
 */
public class itemList extends ArrayAdapter<String[]>{
    private List<String[]> spellList = new ArrayList<String[]>();

    static class ItemViewHolder{
        TextView name;
        TextView spellSchool;
        TextView subSchool;
        TextView descriptor;
        TextView castingTime;
        TextView component;
        TextView costlyComp;
        TextView range;
        TextView area;
        TextView effect;
        TextView targets;
        TextView duration;
        TextView dismissable;
        TextView shapeable;
        TextView savingThrow;
        TextView spellResist;
        TextView description;
        TextView description2;
        TextView source;
        TextView fullText;

    }

    public itemList(Context context, int textViewResourceId){
        super(context, textViewResourceId);
    }

    @Override
    public void add(String[] spell){
        spellList.add(spell);
        super.add(spell);
    }

    @Override
    public int getCount(){
        return this.spellList.size();
    }

    @Override
    public String[] getItem(int index){
        return this.spellList.get(index);
    }

    //@Override
    /*public View getView(int position, View convertView, ViewGroup parent){
        View row = convertView;
        ItemViewHolder viewHolder;
        if(row == null){
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.activity_spell_list, parent, false);
            viewHolder = new ItemViewHolder();
            viewHolder.name = (TextView) row.findViewById(R.id.name);
        }
    }*/

}
